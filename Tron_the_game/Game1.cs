﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Tron_the_game
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D Background;
        Texture2D Bonus1;
        Texture2D Bonus2;
        Texture2D txtEnemy;
        Texture2D txtLadder;
        Texture2D txtMe;
        Texture2D txtWall;
        //Массив для конструирования уровня
        public int[,] Layer;
        Rectangle recBackround = new Rectangle(0, 0, 640, 512);
        Rectangle recSprite = new Rectangle(0, 0, 64, 64);
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Layer = new int[8, 10] {
{ 4, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
{ 1, 5, 1, 3, 5, 1, 1, 2, 1, 5 },
{ 0, 0, 0, 1, 2, 1, 1, 2, 0, 4 },
{ 0, 1, 3, 0, 2, 0, 0, 2, 0, 3 },
{ 0, 5, 0, 0, 1, 1, 1, 2, 0, 1 },
{ 0, 0, 0, 1, 0, 0, 0, 2, 0, 1 },
{ 6, 0, 1, 3, 4, 0, 5, 2, 0, 1 },
{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
};
            //Устанавливаем разрешение игрового окна
            //640х512
            graphics.PreferredBackBufferWidth = 640;
            graphics.PreferredBackBufferHeight = 512;
            graphics.ApplyChanges();
            base.Initialize();
        }
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Services.AddService(typeof(SpriteBatch), spriteBatch);
            Background = Content.Load<Texture2D>("background");
            Bonus1 = Content.Load<Texture2D>("bonus1");
            Bonus2 = Content.Load<Texture2D>("bonus2");
            txtEnemy = Content.Load<Texture2D>("enemy");
            txtLadder = Content.Load<Texture2D>("ladder");
            txtMe = Content.Load<Texture2D>("me");
            txtWall = Content.Load<Texture2D>("wall");
            //Вызываем процедуру расстановки объектов в игровом окне
            AddSprites();
        }
//Процедура расстановки объектов в игровом окне

void AddSprites()
        {
            //Переменные для временного хранения адреса
            //объекта-игрока
            int a = 0, b = 0;
            //Просматриваем массив Layer
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    //Если элемент с индексом (i,j) равен 1 -
                    //устанавливаем в соответствующую позицию элемент с
                    //номером 1, то есть - стену
                    if (Layer[i, j] == 1)
                        Components.Add(new GameObj.Wall(this, ref txtWall, new Vector2(j, i), recSprite));
                    if (Layer[i, j] == 2)
                        Components.Add(new GameObj.Ladder(this, ref txtLadder, new Vector2(j, i), recSprite));
                    if (Layer[i, j] == 3)
                        Components.Add(new GameObj.Bonus1(this, ref Bonus1, new Vector2(j, i), recSprite));
                    if (Layer[i, j] == 4)
                        Components.Add(new GameObj.Bonus2(this, ref Bonus2, new Vector2(j, i), recSprite));
                    if (Layer[i, j] == 5)
                        Components.Add(new GameObj.Enemy(this, ref txtEnemy, new Vector2(j, i), recSprite));
                    //Если обнаружен объект игрока - запишем его координаты
                    if (Layer[i, j] == 6)
                    {
                        a = i;
                        b = j;
                    }
                }
            }
            //Последним установим объект игрока - так он гарантированно
            //расположен поверх всех остальных объектов
            Components.Add(new GameObj.Me(this, ref txtMe, new Vector2(b, a), new Rectangle(0, 0, 32, 32)));
        }
        protected override void UnloadContent()
        {
            Background.Dispose();
            Bonus1.Dispose();
            Bonus2.Dispose();
            txtEnemy.Dispose();
            txtLadder.Dispose();
            txtMe.Dispose();
            txtWall.Dispose();
            spriteBatch.Dispose();
        }
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            //выведем фоновое изображение
            spriteBatch.Draw(Background, recBackround, Color.White);
            //Выведем игровые объекты
            base.Draw(gameTime);
            
        spriteBatch.End();
        }
    }
}
