﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Tron_the_game.GameObj
{
    internal class Bonus2 : IGameComponent
    {
        private Game1 game1;
        private Texture2D txtBonus2;
        private Vector2 vector2;
        private Rectangle recSprite;

        public Bonus2(Game1 game1, ref Texture2D txtBonus2, Vector2 vector2, Rectangle recSprite)
        {
            this.game1 = game1;
            this.txtBonus2 = txtBonus2;
            this.vector2 = vector2;
            this.recSprite = recSprite;
        }

        public void Initialize()
        {
            throw new System.NotImplementedException();
        }
    }
}