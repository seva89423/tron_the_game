﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Tron_the_game.GameObj
{
    internal class Wall : IGameComponent
    {
        private Game1 game1;
        private Texture2D txtWall;
        private Vector2 vector2;
        private Rectangle recSprite;

        public Wall(Game1 game1, ref Texture2D txtWall, Vector2 vector2, Rectangle recSprite)
        {
            this.game1 = game1;
            this.txtWall = txtWall;
            this.vector2 = vector2;
            this.recSprite = recSprite;
        }

        public void Initialize()
        {
            throw new System.NotImplementedException();
        }
    }
}