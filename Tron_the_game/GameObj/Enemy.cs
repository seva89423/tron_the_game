﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Tron_the_game.GameObj
{
    internal class Enemy : IGameComponent
    {
        private Game1 game1;
        private Texture2D txtEnemy;
        private Vector2 vector2;
        private Rectangle recSprite;

        public Enemy(Game1 game1, ref Texture2D txtEnemy, Vector2 vector2, Rectangle recSprite)
        {
            this.game1 = game1;
            this.txtEnemy = txtEnemy;
            this.vector2 = vector2;
            this.recSprite = recSprite;
        }

        public void Initialize()
        {
            throw new System.NotImplementedException();
        }
    }
}