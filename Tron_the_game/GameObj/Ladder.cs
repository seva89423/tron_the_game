﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Tron_the_game.GameObj
{
    internal class Ladder : IGameComponent
    {
        private Game1 game1;
        private Texture2D txtLadder;
        private Vector2 vector2;
        private Rectangle recSprite;

        public Ladder(Game1 game1, ref Texture2D txtLadder, Vector2 vector2, Rectangle recSprite)
        {
            this.game1 = game1;
            this.txtLadder = txtLadder;
            this.vector2 = vector2;
            this.recSprite = recSprite;
        }

        public void Initialize()
        {
            throw new System.NotImplementedException();
        }
    }
}