﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Tron_the_game.GameObj
{
    public class Bonus1 : gBaseClass
    {
        public Bonus1(Game game, ref Texture2D _sprTexture,
        Vector2 _sprPosition, Rectangle _sprRectangle)
        : base(game, ref _sprTexture, _sprPosition, _sprRectangle)
        {
        }
        public override void Initialize()
        {
            base.Initialize();
        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}
